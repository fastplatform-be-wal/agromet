import graphene

from app.api.queries.weather import Weather


class Query(graphene.ObjectType):
    
    meteorology_weather__healthz = graphene.String(default_value="ok")
    weather = graphene.Field(Weather)

    def resolve_weather(self, info):
        return Weather()
