import logging
import httpx
import cachetools
from threading import Lock

from app.settings import config

logger = logging.getLogger(__name__)


class NominatimClient:
    """A client to retrieve data from Nominatim API"""

    def __init__(self):
        self.http_client = None

        self.location_names_cache = cachetools.TTLCache(
            maxsize=config.NOMINATIM_REVERSE_GEOLOCATION_CACHE_MAX_SIZE,
            ttl=config.NOMINATIM_REVERSE_GEOLOCATION_CACHE_TTL,
        )
        self.location_names_cache_lock = Lock()

    def create_http_client(self):
        self.http_client = httpx.AsyncClient()

    async def close_http_client(self):
        await self.http_client.aclose()

    async def get_location_name(self, longitude, latitude):
        """Query location name from the Nominatim API

        This method will return None in case of *any* error.

        Args:
            longitude (float): Longitude in degrees
            latitude (float): Latitude in degrees

        Returns:
            str: Location name (city + country)
        """
        logger.debug("query_nominatim_reverse_geolocation")

        # Reduce the longitude/latitude precision down to roughly 1km
        longitude = round(longitude, 2)
        latitude = round(latitude, 2)

        # Read and return from cache if entry exists
        with self.location_names_cache_lock:
            location_name = self.location_names_cache.get((longitude, latitude))

        if location_name is not None:
            return location_name

        # Otherwise, query from the API
        try:
            response = await self.http_client.get(
                config.NOMINATIM_REVERSE_GEOLOCATION_URL,
                params={"lon": longitude, "lat": latitude, "format": "json"},
                timeout=config.NOMINATIM_REVERSE_GEOLOCATION_DEFAULT_TIMEOUT,
                headers={"User-Agent": config.NOMINATIM_REVERSE_GEOLOCATION_USER_AGENT},
            )
            response.raise_for_status()
            data = response.json()
            location_name = data.get("address", {}).get("city", None)
            if location_name is None:
                location_name = data.get("address", {}).get("municipality", None)
        except Exception as e:
            logger.warning("Failed to retrieve location or to parse it: %s", e)
            return None

        # Set the location_name in the cache
        with self.location_names_cache_lock:
            self.location_names_cache[(longitude, latitude)] = location_name

        return location_name


nominatim_client = NominatimClient()
