
def uvicorn_run_script(name, app, config=None, visibility=None):
    """Instantiate a rule to generate a python script to run an app with uvicorn.

       Instantiated rules 
        - :name to build a Python uvicorn run script
    """
    app_module = app.split(":")[0]
    app_attribute = app.split(":")[1]

    cmd = [
        "sed 's/from module import app as app/from {} import {} as app/g' $<".format(
            app_module,
            app_attribute,
        )
    ]

    if config:
        config_module = config.split(":")[0]
        config_attribute = config.split(":")[1]

        cmd += [
            "sed -e 's/^config[ ]*=.*/from {} import {} as config/g' > $@".format(
                config_module,
                config_attribute,
            )
        ]
    else:
        cmd += [
            "cat - > $@",
        ]

    native.genrule(
        name = name, # unvicorn run
        srcs = [
            "//tools/macros:tpl/uvicorn-run.py"
        ],
        outs = [
            name + ".py"
        ],
        cmd = "|".join(cmd),
        visibility = visibility,
    )
